import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ChangeTrackingAppComponent} from './01-demo/change-tracking-app.component';
import {ChangeTrackingCounterComponent} from './03-cd-service/change-tracking-counter.component';
import {OnPushParentComponent} from './02-on-push/on-push-parent.component';
import {CounterObserverComponent} from './04-cd-service-observable/change-tracking-counter.component';

const routes: Routes = [

  {path: 'demo', component: ChangeTrackingAppComponent},
  {path: 'on-push', component: OnPushParentComponent},
  {path: 'counter', component: ChangeTrackingCounterComponent},
  {path: 'observable', component: CounterObserverComponent},

];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class ChangeTrackingRoutingModule { }
