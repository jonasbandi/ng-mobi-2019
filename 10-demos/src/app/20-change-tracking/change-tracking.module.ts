import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {ChangeTrackingRoutingModule} from './change-tracking-routing.module';
import {ChangeTrackingAppComponent} from './01-demo/change-tracking-app.component';
import {ChangeTrackingFirstChildComponent} from './01-demo/first-child.component';
import {ChangeTrackingOtherChildComponent} from './01-demo/other-child.component';
import {OnPushParentComponent} from './02-on-push/on-push-parent.component';
import {OnPushChildValueComponent} from './02-on-push/on-push-child-value.component';
import {OnPushChildObjectComponent} from './02-on-push/on-push-child-object.component';
import {ChangeTrackingCounterComponent} from './03-cd-service/change-tracking-counter.component';
import {CounterObserverComponent} from './04-cd-service-observable/change-tracking-counter.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ChangeTrackingRoutingModule,
  ],
  declarations: [
    ChangeTrackingAppComponent, ChangeTrackingFirstChildComponent, ChangeTrackingOtherChildComponent,
    OnPushParentComponent, OnPushChildValueComponent, OnPushChildObjectComponent,
    ChangeTrackingCounterComponent,
    CounterObserverComponent
  ]
})
export class ChangeTrackingModule {}
