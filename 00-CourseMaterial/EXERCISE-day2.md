# Angular Wokshop

## Exercise 5: Forms

In the ToDo Application implement a proper form for the entry of a new todo.

Duplicate the component and implement it once with a template driven form and a second time with a reactive form.  
Add a validation that the text should be at least 3 characters.  
As a reference study the examples in `10-demos/src/app/02-forms/`.

*Optional:* Find out how to implement a custom validation: The text should begin with a capital letter. Try to implement that rule for the reactive and for the template driven approach.



## Exercise 6: RxJS - Automated Lessons
Go to the directory `90-RxJS/90-Lessons`.

Start the exercise with:

	npm install
	npm start
	
Complete the lessons by editing the lesson code files: i.e. `01-Basics.js`.
	

## Exercise 7: RxJS - Search Autocomplete

Go to the directory `90-RxJS/91-Search-exercise`.

Start the page with:

	npm install
	npm start
	
Implement the "autocomplete" feature for the Wikipedia search:

- Typing in the search field should update the list below with terms from Wikipedia
- Typing fast: the app should only query Wikipedia when the user makes a pause
- The app should not send the same query to Wikipedia several times
- Ensure that the list always shows the terms for the current input (several http requests are not guaranteed to return in order)


## Exercise 8: RxJS - Numberguess

Go to the directory ``

Start the server in `90-RxJS/_server`:

	npm install
	npm start
	
The sever picks a random number between 1 and 100. You have to guess the number.  
Send a guess to the server: 

	POST http://localhost:3456/numberguess -- Body: { "number": 42 }
	
Retrieve the feedback about the guess:

	GET http://localhost:3456/numberguess
	
The feedback	contains the `status` of the guess: `BELOW`, `ABOVE` or `SUCCESS`
	
Start the client in `99-Observables/95-NumberGuess-exercise`:

	npm install
	npm start
	
The client contains the boilerplate to talk to the server.	
	
### Task 1:	Stateless Client
Implement the functionality for guessing the number:

- The user can submit a guess
- The applications shows if the guess is correct or if it is above or below

*Hint:* A possible solution uses `map` and `switchMap` ...

### Task 2: Brute Force - Fast
The client should try all the numbers from 1-100 and then show the correct number.

*Hint:* To isolate a "guess" from parallel guesses you can pass a `clientId` like this:

	POST http://localhost:3456/numberguess?clientId=12345
	GET http://localhost:3456/numberguess?clientId=12345

*Hint:* A possible solution uses `map`, `mergeMap` and `filter`... `forkJoin` might also help ...	

### Task 3: Brute Force - Sequential
The client should try all the numbers one after the other starting from 1. Once the number is found, no more guesses should be sent.

*Hint:* A possible solution uses `concatMap`, `switchMap`, `map`, `filter` and `take`

### Task 4: Smart Client
The client should efficiently find the number on his own.

*Hints:* 

- The `expand` operator can be used to accomplish something like a loop with an observable.
- Try to avoid any global variables. State should be managed within the streams.
- A possible solution uses `switchMap`, `map`, `expand` and `filter`

### Task 5: Ask User for next number
The user should be asked for a new number until the correct number is found.


## Exercise 9: Backend Access

The directory `40-ToDoApp/_server` contains a simple API-Server implementing basic CRUD functionality for our ToDo application.
Start the server with the following commands:

	npm install #just once
	npm start
	
You should now get an array with two todo items at the url: `http://localhost:3456/todos`.

Your task is now to access this backend API from the ToDo application:
- When the application is loaded, all the todo items should be loaded from the server
- When a todo item is added, it should be saved to the server
- When a todo item  is completed it should be updated on the server
- When a todo item  is deleted, it should be deleted from the server.

Start from `40-ToDoApp/11-Simple-ToDo-backend-exercise`.
This project already loads the  todo items from the server when the application is loaded.


The API implemented by the REST-Endpoint is described in the table below:


HTTP-Method   | URL (example) 		      		| Request-Body  	| Response
------------- | ------------- 			      	|-------------		|-------------
GET	    	  | http://localhost:3456/todos   *(optional query-parameter: ?completed=0 or 1)*| 	| {data: [{*todo*},{*todo*}]}
GET	    	  | http://localhost:3456/todos/1 	|												| {data: {*todo*} }			
POST		  | http://localhost:3456/todos		| { "title": "Test", "completed": false} 		| {data: {*todo*} }
PUT		 	  | http://localhost:3456/todos/1	| { "title": "Test 2", "completed": true}		| *empty*
DELETE		  | http://localhost:3456/todos/1	| 												| *empty*


Note that all responses are wrapped in a response object with a `data` property.
This is a typical security measure of JSON endpoints. See: http://stackoverflow.com/questions/3503102/what-are-top-level-json-arrays-and-why-are-they-a-security-risk

**Hint:**  
Have a look at `10-basic-constructs/src/app/03-BackendAccess/02-backend-crud` to see an implementation of how to access the todo item API endpoint.
 
