# Angular Workshop


## Exercise 10: Module Structure & Bundling

Use the Angular CLI to create a "best practice structure" for a Angular applications (she snippets from the slides):

- Create the root `AppModule` 
- Create the `CoreModule` 
- Create two feature modules 
- Create a shared module
- Add some services and some components to the modules



Make a production build and inspect the build result:

- How many bundles are there? 
- Which components and services are in which bundles? (use `webpack-bundle-analyzer`).

Add some more libraries i.e. components from `primeng` or `momentjs` ... what is the effect on the bundles?


## Exercise 11: Testing

Start with the ToDo Application in `40-ToDoApp/22-Simple-ToDo-testing-exercise`.  
In the directory run:

	npm install
	npm start

Start the server in `40-ToDoApp/_server`:

	npm install
	npm start
	
Run the unit tests:

	npm run test

Run the e2e tests:

	npm run e2e
	
### Tasks: 
- Extend the unit tests with at test that checks that the `ToDoItemComponent` renders a spinner when the ToDo does not have an id (while saving a new ToDo): `src/app/todos/components/todo-item/todo-item.component.spec.ts` (have a look at: `src/app/todos/components/new-todo/new-todo.component.spec.ts`).
- Extend the e2e tests to check that a ToDo item can be deleted on the Done-screen: `e2e/src/app.e2e-spec.ts`.

	

## Exercise 12: Statemangement with a Service

Start with the ToDo Application in `40-ToDoApp/03-Simple-ToDo-backend-solution`.  
In the directory run:

	npm install
	npm start

Start the server in `40-ToDoApp/_server`:

	npm install
	npm start


Introduce a stateful service which manages the ToDos. This service should load the todos from the backend and changes should be saved back to the backend.
The components should become "stateless" and just get the todos from the service.

Optional:

- Try to set the change tracking strategy of the components to 'OnPush'.
- Try to display both lists (pending and done) on the same screen.


## Exercise 13: Intro NgRx

Inspect the app in `40-ToDoApp/06-ToDo-ngrx`.  
Try to understand the flow of component -> action -> store and also the effects.  
In the app you can't delete todo items. Implement this missing functionality by extending the store and the effects.


## Exercise 14: MobX Intro

Inspect the app in `40-ToDoApp/07-ToDo-mobx`.  
Try to understand the angular components react to data changes.  
In the app you can't delete todo items. Implement this missing functionality by extending the store.


	
