describe("Demo", () => {
  let sut;

  beforeEach(function () {
    sut = {};
    sut.mvValue = false;
  });

it("simple assertion should succeed", () => {
  sut.myValue = true;

  expect(sut.myValue).toBe(true);
});

it("aysnc assertion should succeed", (done) => {
  setTimeout(() => sut.myValue = true, 1000);

  setTimeout(() => {
    expect(sut.myValue).toBe(true);
    done();
  }, 6000);
}, 10000);

});
