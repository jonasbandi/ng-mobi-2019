import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {ToDo} from '../../model/todo.model';
import {Observable} from 'rxjs/Observable';
import {IToDosState} from '../../../store/todo.store';
import {Store, createSelector, createFeatureSelector} from '@ngrx/store';
import {CompleteToDo, CreateToDo, LoadToDos} from '../../../store/todo.actions';
import {getPendingToDos, getDoneToDos} from '../../../store/todo.selectors';

@Component({
  selector: 'td-overview',
  templateUrl: './overview.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class OverviewComponent implements OnInit {

  pendingTodos$: Observable<ToDo[]>;
  doneTodos$: Observable<ToDo[]>;

  constructor(private store: Store<{ todos: IToDosState }>) {
    this.pendingTodos$ = this.store.select(getPendingToDos);
    this.doneTodos$ = this.store.select(getDoneToDos);
  }

  ngOnInit(): void {
    this.store.dispatch(new LoadToDos());
  }

  addToDo(todo: ToDo) {
    this.store.dispatch(new CreateToDo(todo));
  }

  completeToDo(todo: ToDo) {
    this.store.dispatch(new CompleteToDo(todo));
  }

}
