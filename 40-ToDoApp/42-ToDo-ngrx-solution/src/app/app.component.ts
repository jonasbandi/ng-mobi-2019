import {Component, OnInit} from '@angular/core';
import {IToDosState} from './store/todo.store';
import {Store} from '@ngrx/store';
import {LoadToDos} from './store/todo.actions';
import {ToDo} from './todos/model/todo.model';
import {Observable} from 'rxjs/Observable';

@Component({
  selector: 'td-root',
  templateUrl: './app.component.html',
})
export class AppComponent implements OnInit {

  message$: Observable<string>;

  constructor(private store: Store<{ message: {message: string} }>) {
    this.message$ = this.store.select('message', 'message');
  }

  ngOnInit(): void {
    // this.store.dispatch(new LoadToDos());
  }
}
