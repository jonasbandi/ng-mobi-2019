import {ToDo} from '../todos/model/todo.model';
import {COMPLETE_TODO, CREATE_TODO_SUCCESS, LOAD_TODOS_SUCCESS, REMOVE_TODO, ToDoAction} from './todo.actions';

export interface IToDosState {
  pendingTodos: ToDo[];
  doneTodos: ToDo[];
}

export const initialState: IToDosState = {
    pendingTodos: [],
    doneTodos: []
  };

export function todoReducer(state = initialState, action: ToDoAction): IToDosState {
  switch (action.type) {
    case LOAD_TODOS_SUCCESS: {
      const pendingTodos = action.payload.filter(t => !t.completed);
      const doneTodos = action.payload.filter(t => t.completed);
      return {...state, pendingTodos, doneTodos};
    }
    case CREATE_TODO_SUCCESS: {
      const pendingTodos = [...state.pendingTodos, action.payload];
      return {...state, pendingTodos};
    }
    case COMPLETE_TODO: {
      const todo = action.payload;
      todo.completed = true;
      const pendingTodos = state.pendingTodos.filter(t => t !== todo);
      const doneTodos = [...state.doneTodos, todo];
      return {...state, pendingTodos, doneTodos};
    }
    case REMOVE_TODO: {
      const todo = action.payload;
      const doneTodos = state.doneTodos.filter(t => t !== todo);
      return {...state, doneTodos};
    }
    default:
      return state;
  }
}


