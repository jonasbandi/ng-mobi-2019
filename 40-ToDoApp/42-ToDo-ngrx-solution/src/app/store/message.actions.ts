import {Action} from '@ngrx/store';
import {ToDo} from '../todos/model/todo.model';

export const MESSAGE_SHOW = '[Message] Show';

export class ShowMessage implements Action {
  readonly type = MESSAGE_SHOW;
  constructor(public payload: any) {
  }
}

export type MessageAction =
  | ShowMessage;
