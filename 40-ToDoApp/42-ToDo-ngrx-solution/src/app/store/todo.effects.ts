import {Injectable} from '@angular/core';
import {Effect, Actions} from '@ngrx/effects';
import {ToDoService} from '../todos/model/todo.service';
import {map, switchMap, catchError, tap, mergeMap, concatMap} from 'rxjs/operators';
import {of} from 'rxjs/observable/of';
import {
  COMPLETE_TODO, CompleteToDo, CompleteToDoFail, CompleteToDoSuccess, CREATE_TODO, CreateToDo, CreateToDoFail, CreateToDoSuccess,
  LOAD_TODOS, LoadToDosFail,
  LoadToDosSuccess, REMOVE_TODO, RemoveToDo, RemoveToDoFail, RemoveToDoSuccess
} from './todo.actions';
import {ShowMessage} from './message.actions';
import {Store} from '@ngrx/store';
import {IToDosState} from './todo.store';

@Injectable()
export class ToDoEffects {
  constructor(private actions$: Actions,
              private toDoService: ToDoService,
              private store: Store<IToDosState>) {
  }

  @Effect()
  loadToDos$ = this.actions$.ofType(LOAD_TODOS).pipe(
    tap(() => this.store.dispatch(new ShowMessage('Loading ...'))),
    switchMap(() => {
      return this.toDoService
        .getTodos()
        .pipe(
          mergeMap(todos => [
            new LoadToDosSuccess(todos),
            new ShowMessage('')
          ]),
          catchError(error => of(new LoadToDosFail(error)))
        );
    })
  );

  @Effect()
  createToDo$ = this.actions$.ofType(CREATE_TODO).pipe(
    tap(() => this.store.dispatch(new ShowMessage('Saving ...'))),
    map((action: CreateToDo) => action.payload),
    concatMap(todo => {
      return this.toDoService
        .saveTodo(todo)
        .pipe(
          map(savedToDo => (new CreateToDoSuccess(savedToDo))),
          catchError(error => of(new CreateToDoFail(error)))
        );
    })
  );

  @Effect()
  updateToDo$ = this.actions$.ofType(COMPLETE_TODO).pipe(
    tap(() => this.store.dispatch(new ShowMessage('Updating ...'))),
    map((action: CompleteToDo) => action.payload),
    concatMap(todo => {
      todo.completed = true;
      return this.toDoService
        .updateTodo(todo)
        .pipe(
          map(() => (new CompleteToDoSuccess())),
          catchError(error => of(new CompleteToDoFail()))
        );
    })
  );

  @Effect()
  removeToDo$ = this.actions$.ofType(REMOVE_TODO).pipe(
    tap(() => this.store.dispatch(new ShowMessage('Deleting ...'))),
    map((action: RemoveToDo) => action.payload),
    concatMap(todo => {
      todo.completed = true;
      return this.toDoService
        .deleteTodo(todo)
        .pipe(
          map(() => (new RemoveToDoSuccess())),
          catchError(error => of(new RemoveToDoFail()))
        );
    })
  );

}
