import {MESSAGE_SHOW, MessageAction} from './message.actions';
import {COMPLETE_TODO_SUCCESS, CREATE_TODO_SUCCESS, REMOVE_TODO_SUCCESS, ToDoAction} from './todo.actions';

export interface IMessageState {
  message: string;
}

const initialState: IMessageState = {message: ''};


export function messageReducer(state = initialState, action: MessageAction | ToDoAction) {
  switch (action.type) {
    case MESSAGE_SHOW: {
      return {...state, message: action.payload};
    }
    case CREATE_TODO_SUCCESS:
    case COMPLETE_TODO_SUCCESS:
    case REMOVE_TODO_SUCCESS: {
      return {...state, message: ''};
    }

    default:
      return state;
  }
}


