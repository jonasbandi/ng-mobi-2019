import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// import {OverviewComponent} from './todos/components/todo-screen/overview.component';


const routes: Routes = [
  { path: '', pathMatch: 'full', loadChildren: './todos/components/todo-screen/todo-screen.module#ToDoScreenModule' },
  { path: 'done', loadChildren: './todos/components/done-screen/done-todos.module#DoneScreenModule' },
  { path: 'details', loadChildren: './todos/components/detail-screen/detail-screen.module#DetailScreenModule' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

