import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {TodoScreenModule} from './todos/components/todo-screen/todo-screen.module';
import { DoneScreenModule } from './todos/components/done-screen/done-screen.module';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    TodoScreenModule,
    DoneScreenModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
