import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { ToDo } from '../../model/todo.model';
import { ToDoStoreService } from '../../model/todo-store.service';

@Component({
  selector: 'td-todos',
  templateUrl: './todo-screen.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TodoScreenComponent implements OnInit {
  constructor(private todoService: ToDoStoreService) {}

  ngOnInit() {
  }

  get todos() {
    return this.todoService.pendingTodos;
  }

  addToDo(todo: ToDo) {
    this.todoService.addTodo(todo);
  }

  completeToDo(todo: ToDo) {
    const updated = { ...todo, completed: true };
    this.todoService.updateTodo(updated);
  }
}
