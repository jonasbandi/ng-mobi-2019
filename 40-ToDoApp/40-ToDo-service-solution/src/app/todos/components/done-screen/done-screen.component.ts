import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { ToDo } from '../../model/todo.model';
import { ToDoApiService } from '../../model/todo-api.service';
import { ToDoStoreService } from '../../model/todo-store.service';

@Component({
  selector: 'td-done-todos',
  templateUrl: './done-screen.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DoneScreenComponent implements OnInit {
  constructor(private todoService: ToDoStoreService) {}

  ngOnInit() {
  }

  get todos() {
    return this.todoService.completedTodos;
  }

  removeToDo(todo: ToDo) {
    this.todoService.deleteTodo(todo);
  }
}
