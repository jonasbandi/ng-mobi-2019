import { FormControl } from '@angular/forms';

export const validators = {
  validateTodo(c: FormControl) {
    const firstChar = c.value && c.value.charAt(0);

    if (!firstChar || firstChar === firstChar.toUpperCase()) {
      return null;
    } else {
      return { capitalLetter: { valid: false } };
    }
  }
};

export function getValidators() {
  return validators;
}
