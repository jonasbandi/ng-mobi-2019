import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { HelloComponent } from './hello/hello.component';
import { AsyncComponent } from './async/async.component';
import { DependentComponent } from './dependent/dependent.component';

@NgModule({
  declarations: [
    AppComponent,
    HelloComponent,
    AsyncComponent,
    DependentComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
