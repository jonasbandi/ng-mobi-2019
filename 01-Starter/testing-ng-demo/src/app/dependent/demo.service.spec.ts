import {async, TestBed} from '@angular/core/testing';

import {DemoService} from './demo.service';
import {HttpClient} from '@angular/common/http';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {of} from 'rxjs';

describe('DemoService', () => {
  beforeEach(() =>
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    })
  );

  it('should return data from HttpClientTestingModule', () => {
    const service: DemoService = TestBed.get(DemoService);
    const httpTestingController: HttpTestingController = TestBed.get(HttpTestingController);

    const testData = {name: 'Luke'};

    service.getLuke().subscribe(data => expect(data).toEqual(testData));

    const req = httpTestingController.expectOne('https://swapi.co/api/people/1/');
    expect(req.request.method).toEqual('GET');

    req.flush(testData);
  });

  it('should return data from custom mock', () => {
    const httpClientSpy = jasmine.createSpyObj('httpClient', ['get']);

    const testData = {name: 'Luke'};
    httpClientSpy.get.and.returnValue(of(testData));

    const service: DemoService = new DemoService(httpClientSpy);

    service.getLuke().subscribe(data => expect(data).toEqual(testData));
  });
});
