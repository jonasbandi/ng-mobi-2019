import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DemoService {
  constructor(private httpClient: HttpClient) {}

  getLuke(): Observable<{}> {
    return this.httpClient.get('https://swapi.co/api/people/1/');
  }
}
