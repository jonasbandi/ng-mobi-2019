import {TestBed, async} from '@angular/core/testing';
import {By} from '@angular/platform-browser';
import {AppComponent} from './app.component';
import {HelloComponent} from './hello/hello.component';
import {FormsModule} from '@angular/forms';
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('AppComponent', () => {


  describe('shallow integrated test (with template)', () => {

    beforeEach(
      () => {
        TestBed.configureTestingModule({
          declarations: [AppComponent],
          schemas: [ NO_ERRORS_SCHEMA ]
        });
      }
    );
    it('should render title', () => {
      const fixture = TestBed.createComponent(AppComponent);
      fixture.detectChanges();
      const domElement = fixture.debugElement.nativeElement;
      expect(domElement.querySelector('h1').textContent).toContain('Welcome to app!');    });
  });

  describe('deep integrated test (with child components)', () => {
    beforeEach(
      async(() => {
        TestBed.configureTestingModule({
          declarations: [AppComponent, HelloComponent],
          imports: [FormsModule]
        });
      })
    );

    it('should render title and message', () => {
        const fixture = TestBed.createComponent(AppComponent);
        fixture.detectChanges();
        const domElement = fixture.debugElement.nativeElement;
        expect(domElement.querySelector('h1').textContent).toContain('Welcome to app!');
        expect(domElement.querySelector('#greeting-message').textContent).toContain('World');
      }
    );
  });
});
