import {async, ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';

import {AsyncComponent} from './async.component';

describe('AsyncComponent', () => {
  let fixture: ComponentFixture<AsyncComponent>;
  let component: AsyncComponent;

  beforeEach(
    () => {
      TestBed.configureTestingModule({
        declarations: [AsyncComponent]
      });
    }
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(AsyncComponent);
    component = fixture.componentInstance;
  });

  it('should update message after a while (fakeAsync)', fakeAsync(() => {
      fixture.detectChanges();
      tick(2025);
      expect(component.message).toEqual('Updated Message');
    })
  );

  it('should update message after a while (async)', async(() => {
      fixture.detectChanges();
      return fixture.whenStable().then(() => {
        expect(component.message).toEqual('Updated Message');
      });
    })
  );

});
