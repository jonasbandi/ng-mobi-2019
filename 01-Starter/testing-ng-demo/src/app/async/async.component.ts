import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-async-component',
  templateUrl: './async.component.html',
  styleUrls: ['./async.component.css']
})
export class AsyncComponent implements OnInit {
  message: string;

  constructor() {}

  ngOnInit() {
    setTimeout(() => {
      this.message = 'Updated Message';
    }, 2000);
  }
}
