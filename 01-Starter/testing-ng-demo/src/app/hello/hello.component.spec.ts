import { TestBed, async, fakeAsync, ComponentFixture, ComponentFixtureAutoDetect } from '@angular/core/testing';
import { By } from '@angular/platform-browser';

import { HelloComponent } from './hello.component';
import { FormsModule } from '@angular/forms';

describe('HelloComponent', () => {
  describe('isolated test (no template)', () => {
    it(`should have a default greeting name`, () => {
      const component = new HelloComponent();
      expect(component.greetingName).toEqual('World');
    });

    it('component should trim greeting name', () => {
      const component = new HelloComponent();
      component.greetingName = ' Gugus Hello  ';
      expect(component.greetingName).toEqual('Gugus Hello');
    });
  });

  describe('shallow integrated test (with template)', () => {
    let fixture: ComponentFixture<HelloComponent>;
    let component: HelloComponent;

    beforeEach(() => {
      TestBed.configureTestingModule({
        declarations: [HelloComponent],
        imports: [FormsModule],
      });

      fixture = TestBed.createComponent(HelloComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();
    });

    it(`should initially render the default greeting`, () => {
      const domElement = fixture.debugElement.nativeElement;
      expect(domElement.textContent).toContain('Hello, World!');
    });

    it(`should update the greeting`, () => {
      const newGreetingName = 'Test';
      const inputDomElement = fixture.debugElement.query(By.css('#greeting-input')).nativeElement;
      inputDomElement.value = newGreetingName;

      // dispatch a DOM event so that Angular learns of input value change...  leaky abstraction !?!
      inputDomElement.dispatchEvent(new Event('input'));
      expect(component.greetingName).toEqual(newGreetingName);

      fixture.detectChanges();
      const messageDomElement = fixture.debugElement.query(By.css('#greeting-message')).nativeElement;
      expect(messageDomElement.textContent).toContain(newGreetingName);
    });
  });
});
