import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-hello',
  templateUrl: './hello.component.html',
  styleUrls: ['./hello.component.css']
})
export class HelloComponent implements OnInit {

  private _greetingName = 'World';

  constructor() {
  }

  ngOnInit() {
  }

  get greetingName() {
    return this._greetingName;
  }

  set greetingName(val: string) {
    this._greetingName = val.trim();
  }
}
